<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php include ("HeaderMaster.php");?>
    <script src="javascript/scriptcadastrocarteirinha.js" defer></script>
</head>
<body>
    <header>
    Manutencao de Carteirinha
   

    </header>
    <section id="frmCadastrarCarteirinha" class="container">
        <div class="form">
            <div class="form-group row">    
               <label>Nome:</label>
               <label id="lblNome"></label>
               <label>Data de Nascimento:</label> 
                <label id="lblDataNascimento"></label>
            </div> 
           
            <section id="mapaCarteirinha" class="container">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="tblCarteirinha">
                    </table>
                </div>
                  <!-- Modal -->
                  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document" >
                        <div class="modal-content"> 
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"></h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="modalBody" class="container" >
                            
                        </div>
                        <div class="modal-footer">
                            <button type="Button" class="btn btn-success" data-dismiss="modal">Fechar</button>
                            <button id="btnCriarDose" type="Button" class="btn btn-success">Salvar</button>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div>
                <button  id="btnSair" type="Button" class="btn btn-success">Sair</button>
                </div>
            </section>   
        
    </section>

</body>


</html>