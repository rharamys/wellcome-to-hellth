<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php include ("HeaderMaster.php");?>
    <script src="javascript/scriptcadastrousuario.js" defer></script>
</head>
<body>
    <header>Cadastro de Usuario</header>
    <section id="Cadastro" class="container">
        <div class="form">
            <div class="form-group">    
            <label>Nome</label>
            <input id="txtNome" type="text" required=true class="form-control"/>
            </div>
            <div class="form-group">
            <label>E-mail</label>
            <input id="txtEmail" type="text" required=true class="form-control"/>
            </div>
            <div class="form-group">
            <label>Telefone</label>
            <input id="txtTelefone" required=true class="form-control phone-mask" placeholder="Ex.: 0000-0000" type="text"/>
            </div>
            <div class="form-group">
            <label>Data de Nascimento</label>
            <input id="txtDataNascimento" class="form-control date-time-mask" placeholder="Ex.: 00/00/0000" type="text"/>
            </div>
            <div class="form-group">
            <label for="txtSenha">Senha</label>
            <input id="txtSenha" type="password"  required=true class="form-control"/>
            </div>
        </div>
        <section id="botoes">
            <div>
               <button id="btnVoltar" class="btn btn-success">Voltar</button>
                <button id="btnCadastrar" class="btn btn-success">Criar Usuario</button>
            </div>
        </section>
    </section>
</body>
</html>