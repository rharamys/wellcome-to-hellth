const btnCadastrar = document.querySelector("#btnCadastrar");
const btnVoltar = document.querySelector("#btnVoltar");

function telaLogin(){
    window.location.href = "login.php";
}

function telaCadastrar(){
    window.location.href = "cadastroUsuario.php";
}

function redirecionarCadastroCarteirinha(dados){

    console.log("Dados final :");    
    console.log(dados);    


    window.location.href = "cadastroCarteirinha.php";

}

function CadastrarUsuario(){

    let data = { 
        email : document.querySelector("#txtEmail").value, 
        nome : document.querySelector("#txtNome").value,
        senha: document.querySelector("#txtSenha").value,
        telefone : document.querySelector("#txtTelefone").value,
        dataNascimento : document.querySelector("#txtDataNascimento").value
    };
   
    post("usuario/add", data).then(telaPrincipalLogar).catch(redirecionaErro);

}

function telaPrincipalLogar(dados){

    //console.log(document.querySelector("#txtEmail").value);
        let data = { 
            email : document.querySelector("#txtEmail").value, 
            senha: document.querySelector("#txtSenha").value
        };
        
        post("usuario/login", data).then(novaCarteirinha).catch(redirecionaErro);

}

function novaCarteirinha(data){
    //Cria a carteirinha
    let dados = { 
        email : data.email, 
        tipoCarteirinha : "Normal"
    };

    console.log("Dados carteirinha :");    
    console.log(dados);    

    post("carteirinha", dados).then(redirecionarCadastroCarteirinha).catch(redirecionaErro);

}

function redirecionaErro(dados)
{
    //console.log(dados);
    alert("deupau");
}



btnCadastrar.onclick = CadastrarUsuario;
btnVoltar.onclick = telaLogin;
