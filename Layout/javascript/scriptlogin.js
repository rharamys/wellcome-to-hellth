    
        const btnCadastrar = document.querySelector("#btnCadastrar");
        const btnEntrar = document.querySelector("#btnEntrar");
        const btnVoltar = document.querySelector("#btnVoltar");

        function telaLogin(){
            window.location.href = "login.php";
        }

        function telaCadastrar(){
            window.location.href = "cadastroUsuario.php";
        }

        function telaPrincipalLogar(){

            let data = { 
                email : document.querySelector("#txtEmail").value, 
                senha: document.querySelector("#txtSenha").value
            };
            
            post("usuario/login", data).then(redirecionarPrincipal).catch(redirecionaErro);
        }


        function redirecionarPrincipal(dados){
            console.log(dados);
        
     
                window.location.href = "cadastroCarteirinha.php";
       
        }

        function redirecionaErro(){

            alert('Usuario e/ou senha nao cadastrado');

        }

        btnCadastrar.onclick = telaCadastrar;
        btnEntrar.onclick = telaPrincipalLogar;