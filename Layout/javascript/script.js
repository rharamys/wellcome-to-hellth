const urlMasterApi = "http://142.93.23.112:8080/";

function get(endpoint){
        return fetch(urlMasterApi + endpoint, {
            method: 'get',
            headers: {
                'Authorization': localStorage.getItem('token'), 
                'content-type': 'application/json',
                'Access-Control-Allow-Origin': "*",
                "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
            }
        }).then((response) => {
            return response.json();
        });
    }

function post(endpoint, data){
    return fetch(urlMasterApi + endpoint,
    {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Authorization': localStorage.getItem('token'), 
            'content-type': 'application/json',
            'Access-Control-Allow-Origin': "*",
            "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
        }
    }).then((response) => {
        if(response.headers.get('Authorization')){
            localStorage.setItem('token', response.headers.get('Authorization'));
        }

        return response.json();
    });
}
    
    function put(endpoint, data){
        return fetch(urlMasterApi + endpoint,
        {
            method: 'put',
            body: JSON.stringify(data),
            headers: {
                'Authorization': localStorage.getItem('token'), 
                'content-type': 'application/json',
                'Access-Control-Allow-Origin': "*",
                "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
            }
        }).then((response) => {
            if(response.headers.get('Authorization')){
                localStorage.setItem('token', response.headers.get('Authorization'));
            }
    
            return response.json();
        });
    
        
    }

    
    function logout(){

        localStorage.setItem('token', "");

        window.location = "login.php";

    }