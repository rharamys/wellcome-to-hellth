    
        const btnVerificar = document.querySelector("#btnVerificar");
        const btnNovaCarteirinha = document.querySelector("#btnNovaCarteirinha");
        const btnSair = document.querySelector("#btnSair");
        const btnNaoObrigado = document.querySelector("#btnNaoObrigado");
        const frmNovaCarteirinha = document.querySelector('#frmNovaCarteirinha');
        const frmCadastrarCarteirinha = document.querySelector('#frmCadastrarCarteirinha');
        const mapaCarteirinha = document.querySelector('#mapaCarteirinha');
        const tblCarteirinha = document.querySelector('#tblCarteirinha');
        const cmbTipo = document.querySelector('#cmbTipo');
        let dadosUsuario;
        let carteirinha;
    

        function telaPrincipalLogar(){tblCarteirinha
             window.location.href = "ttblCarteirinhacipal.php";
        }

        function MontaCabecalho(){
            //Cria Cabecalho
            let thead = document.createElement('thead');
                
            //Cria Linha
            let tr = document.createElement('tr');
            
            //Cria a primeira coluna com #
            let th = document.createElement('th');
            th.innerHTML = "#";
            tr.appendChild(th);

            for(let vacina of carteirinha.vacinaRetornos){
                //Cria a primeira coluna com #
                let th1 = document.createElement('th');
                th1.innerHTML = vacina.nome;
                tr.appendChild(th1);
            }     

            thead.appendChild(tr)

            return thead;
        }

        function MontaLinhas(){
            //Cria corpo
            let tbody = document.createElement('tbody');

            for(let x = 1; x < 11; x++){
                //Cria Linha
                let tr = document.createElement('tr');
                tr.innerHTML = "<pre>Dose. " + x+"</pre>" ;
                
                for(let vacina of carteirinha.vacinaRetornos){
                    let th1 = document.createElement('th');
                    let cont = 1;
                    for(let dose of vacina.doses){
                        
                        if(x == cont){

                            th1.appendChild(MontaVacina(vacina , dose, true));

                        }
                        cont ++;
                    }               

                    tr.appendChild(th1);
                }     

                tbody.appendChild(tr);
            }
          
            return tbody;
        }

        function MontaVacina(vacina, dose, modal){
            
            let tblVacina = document.createElement('table');

    
            //if(dose.dataInjecao == ""){
            
                tblVacina.setAttribute("data-toggle" , "modal");
                tblVacina.setAttribute("data-target","#exampleModal");

                tblVacina.onclick = montaModalVacina;
           
           // }
        

             //Monta a data da vacina  
       

            tblVacina.dados = vacina;
            
            tblVacina.dose = dose;

            tblVacina.appendChild(MontaTrdataInjecao(dose.dataInjecao, false));

            //Monta o lote 
            tblVacina.appendChild(MontaTrLote(dose.lote, false));

            //Monta o nomeVacinador 
            tblVacina.appendChild(MontaTrnomeVacinador(dose.nomeVacinador, false));

            //Monta a laboratorio 
            tblVacina.appendChild(MontaTrlaboratorio(dose.laboratorio, false));

            return tblVacina;

        }


        function montaModalVacina(){

         
            let nomeModal = document.querySelector('#exampleModalLabel');
            let modalBody = document.querySelector('#modalBody');
            let btnCriarVacina = document.querySelector('#btnCriarVacina');
           
        
            let vacina = this.dados;
            let dose = this.dose;
            nomeModal.innerHTML = vacina.nome ;
            modalBody.innerHTML = "";

            let sessionModal = document.createElement("section");
            sessionModal.setAttribute("class", "container");
            
            let divModal = document.createElement("div");
            divModal.setAttribute("class", "form");
            

            //Monta a data da vacina  
            divModal.appendChild(MontaTrdataInjecao(this.dose.dataInjecao, true));

            //Monta o lote 
            divModal.appendChild(MontaTrLote(this.dose.lote, true));

            //Monta o nomeVacinador 
            divModal.appendChild(MontaTrnomeVacinador(this.dose.nomeVacinador, true));

            //Monta a laboratorio 
            divModal.appendChild(MontaTrlaboratorio(this.dose.laboratorio, true));

            btnCriarDose.onclick = criarDose;
            
            btnCriarDose.dados = vacina;

            btnCriarDose.dose = dose;

            sessionModal.appendChild(divModal);

            modalBody.appendChild(sessionModal);
           
        }

        function criarDose(){

            let modalBody = document.querySelector('#modalBody');

            let data = { 
                vacinaUuid: this.dados.vacinaUuid,
                doseUuid: this.dose.doseUuid,
                tomada: true,
                dataInjecao : document.querySelector("#txtdataInjecao").value, 
                lote : document.querySelector("#txtLote").value,
                nomeVacinador: document.querySelector("#txtnomeVacinador").value,
                laboratorio : document.querySelector("#txtlaboratorio").value
            };


            put("dose/" + this.dose.doseUuid, data).then(MontaCartela);
          
        }

        function MontaTrdataInjecao(dataInjecao ,modal){


            if(!modal){
                let tr = document.createElement('btnSair');
            
                if(dataInjecao !== "")
                {
                    tr.innerHTML = dataInjecao;
                }else
                {
                    tr.innerHTML = "__/__/__";
                }
                return tr;

            }else{

                let tr = document.createElement('btnSair');
                let th = document.createElement("th");
                let input = document.createElement("input");
                input.id = "txtdataInjecao";
                
                th.innerHTML = "Data Vacinacao: " ;
               
           
                input.value = dataInjecao;

                th.appendChild(input);
                tr.appendChild(th);
                return tr;

            }

        }

        

  
        
        function MontaTrLote(lote, modal){
            
            if(!modal){
                let tr = document.createElement('tr');
                if(lote !== 0)
                {
                    tr.innerHTML = "Lote:  " +  lote;
                }else
                {
                    tr.innerHTML = "Lote:_____"; 
                }
                return tr;

            }else{

                let tr = document.createElement('tr');
                let th = document.createElement("th");
                let input = document.createElement("input");
                input.id = "txtLote";
                th.innerHTML = "Lote: " ;
               
                input.value = lote;

                th.appendChild(input);
                tr.appendChild(th);
                return tr;

            }
        }

        function MontaTrnomeVacinador(nomeVacinador, modal){

            if(!modal){
                let tr = document.createElement('tr');
                if(nomeVacinador !== null)
                {
                    tr.innerHTML = "Func:  " +  nomeVacinador;
                }else
                {
                    tr.innerHTML = "Func:_____ "; 
                }
                return tr;

            }else{

                let tr = document.createElement('tr');
                let th = document.createElement("th");
                let input = document.createElement("input");
                
                th.innerHTML = "Vacinador: " ;
                input.id = "txtnomeVacinador";
                input.value = nomeVacinador;
                th.appendChild(input);
                tr.appendChild(th);
                return tr;

            }
        }

        function MontaTrlaboratorio(laboratorio, modal){

            if(!modal){
                let tr = document.createElement('tr');
                if(laboratorio !== null)
                {
                    tr.innerHTML = "Unid:  " +  laboratorio;
                }else
                {
                    tr.innerHTML = "Unid:_____"; 
                }
                return tr;

            }else{

                let tr = document.createElement('tr');
                let th = document.createElement("th");

                let input = document.createElement("input");
                
                th.innerHTML = "Laboratorio: " ;
                input.id = "txtlaboratorio";
                input.value = laboratorio;
               
                th.appendChild(input);
                tr.appendChild(th);
                return tr;

            }
        }

        function recuperaCartela(){

            
            getdadosUsuario();

        
        }

        function getdadosUsuario(){

            get("usuario/sessao").then(carregaJsonDadosUsuario).catch(redirecionErro);
            
        }
        function redirecionErro(){

            window.location.href = "login.php";

        }

        
        function carregaJsonDadosUsuario(dados){
            
            if(dados.nome !== ""){
                document.querySelector('#lblNome').innerHTML = dados.nome;

            }

            if(dados.dataNascimento !== ""){
                document.querySelector('#lblDataNascimento').innerHTML = dados.dataNascimento;

            }

            dadosUsuario = dados;

            

            get("carteirinha/"+dadosUsuario.email).then(carregaCarteirinha);
            
        }

        function carregaCarteirinha(dados){

        

            carteirinha = dados;
       
           
            
            //Monta o cabecalho
            tblCarteirinha.appendChild(MontaCabecalho());

            //Monta as linhas
            tblCarteirinha.appendChild(MontaLinhas());

        }


        function MontaCartela(){
       
            tblCarteirinha.innerHTML = "";

            recuperaCartela();

        }


        MontaCartela();
    
       btnSair.onclick = logout;