package com.itau.welcometohellth.repositories;

import java.util.UUID;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.core.mapping.CassandraPersistentEntity;
import org.springframework.data.cassandra.repository.query.CassandraEntityInformation;
import org.springframework.data.cassandra.repository.support.MappingCassandraEntityInformation;

import com.itau.welcometohellth.models.Dose;
import com.itau.welcometohellth.models.Vacina;

@Configuration
public class VacinaConfig {
	
	@Bean
	public VacinaRepository vacinaRepository(
			CassandraTemplate cassandraTemplate) {
		
		CassandraPersistentEntity<?> entity = cassandraTemplate
				.getConverter()
				.getMappingContext()
				.getRequiredPersistentEntity(Vacina.class);
		
		CassandraEntityInformation
		<Vacina, UUID> metadata = new MappingCassandraEntityInformation<>(
				(CassandraPersistentEntity<Vacina>)entity, cassandraTemplate.getConverter());
		
		return new VacinaRepositoryImpl(metadata, cassandraTemplate);
	}
}