package com.itau.welcometohellth.repositories;

import java.util.UUID;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.cassandra.core.CassandraTemplate;
import org.springframework.data.cassandra.core.mapping.CassandraPersistentEntity;
import org.springframework.data.cassandra.repository.query.CassandraEntityInformation;
import org.springframework.data.cassandra.repository.support.MappingCassandraEntityInformation;

import com.itau.welcometohellth.models.Dose;

@Configuration

public class DoseConfig {
	
	@Bean
	public DoseRepository postRepository(
			CassandraTemplate cassandraTemplate) {
		
		CassandraPersistentEntity<?> entity = cassandraTemplate
				.getConverter()
				.getMappingContext()
				.getRequiredPersistentEntity(Dose.class);
		
		CassandraEntityInformation
		<Dose, UUID> metadata = new MappingCassandraEntityInformation<>(
				(CassandraPersistentEntity<Dose>)entity, cassandraTemplate.getConverter());
		
		return new DoseRepositoryImpl(metadata, cassandraTemplate);
	}
}

