package com.itau.welcometohellth.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraBatchOperations;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.repository.query.CassandraEntityInformation;
import org.springframework.data.cassandra.repository.support.SimpleCassandraRepository;

import com.itau.welcometohellth.models.Dose;
import com.itau.welcometohellth.models.VacinaDose;

public class DoseRepositoryImpl extends SimpleCassandraRepository<Dose, UUID> implements DoseRepository{
	private final CassandraOperations operations;
	
	@Autowired
	VacinaDoseRepository vacinaDoseRepository;
	
	public DoseRepositoryImpl(CassandraEntityInformation<Dose, UUID> metadata, CassandraOperations operations) {
		super(metadata, operations);
		this.operations = operations;
	}

	public <S extends Dose> S deletar(UUID vacinaUuid, UUID doseUuid) {
		
		Optional<Dose> doseEncontrada = findById(doseUuid);
		if (!doseEncontrada.isPresent()) {
			return null;
		}
		
		Optional<VacinaDose> vacinaDoseEncontrada = vacinaDoseRepository.findByVacinaUuidAndDoseUuid(vacinaUuid, doseUuid);
		if (!vacinaDoseEncontrada.isPresent()) {
			return null;
		}
		
		CassandraBatchOperations ops = this.operations.batchOps();
		
		ops.delete(vacinaDoseEncontrada.get());
		ops.delete(doseEncontrada.get());
		ops.execute();
		
		return (S)doseEncontrada.get();
	}
	
	public <S extends Dose> S insert(UUID uuid, S dose) {
		VacinaDose vacinaDose = new VacinaDose();
		vacinaDose.setTomada(dose.isTomada());
		vacinaDose.setDataDose(dose.getDataDose());
		vacinaDose.setDataInjecao(dose.getDataInjecao());
		vacinaDose.setVacinaUuid(uuid);
		vacinaDose.setDoseUuid(dose.getDoseUuid());
		vacinaDose.setLaboratorio(dose.getLaboratorio());
		vacinaDose.setLote(dose.getLote());
		vacinaDose.setNomeVacinador(dose.getNomeVacinador());
		
		CassandraBatchOperations ops = this.operations.batchOps();
		
		ops.insert(dose);
		ops.insert(vacinaDose);
		ops.execute();
		
		return dose;
	}
}

