package com.itau.welcometohellth.repositories;

import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.itau.welcometohellth.models.Dose;

@NoRepositoryBean
public interface DoseRepository extends CassandraRepository<Dose, UUID>{

	public <S extends Dose> S insert(UUID uuid, S entity);

	public <S extends Dose> S deletar(UUID vacinaUuid, UUID doseUuid);

}