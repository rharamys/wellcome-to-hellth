package com.itau.welcometohellth.repositories;

import java.util.Optional;
import org.springframework.data.cassandra.core.mapping.Table;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.itau.welcometohellth.models.Usuario;

@Table
@CrossOrigin
public interface UsuarioRepository extends CrudRepository<Usuario, String> {

	public Optional<Usuario> findByEmail(String email);
	public void deleteByEmail(String email);
	
}
