package com.itau.welcometohellth.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.welcometohellth.models.CarteirinhaVacina;

public interface CarteirinhaVacinaRepository extends CrudRepository<CarteirinhaVacina, String>{

	public List<CarteirinhaVacina> findAllByUserEmail (String userEmail);
	public Optional<CarteirinhaVacina> findByUserEmailAndVacinaUuid(String userEmail, UUID vacinaUUID);
}
