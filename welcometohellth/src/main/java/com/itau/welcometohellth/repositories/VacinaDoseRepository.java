package com.itau.welcometohellth.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.welcometohellth.models.VacinaDose;

public interface VacinaDoseRepository extends CrudRepository<VacinaDose, UUID>{

	public List<VacinaDose> findAllByVacinaUuid(UUID vacinaUuid);
	
	public Optional<VacinaDose> findByVacinaUuidAndDoseUuid(UUID vacinaUuid, UUID doseUuid);
	
}
