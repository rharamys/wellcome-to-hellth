package com.itau.welcometohellth.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.welcometohellth.models.TiposCarteirinha;

public interface TiposCarteirinhaRepository extends CrudRepository<TiposCarteirinha, UUID> {

}
