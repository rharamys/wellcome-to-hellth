package com.itau.welcometohellth.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.cassandra.core.CassandraBatchOperations;
import org.springframework.data.cassandra.core.CassandraOperations;
import org.springframework.data.cassandra.repository.query.CassandraEntityInformation;
import org.springframework.data.cassandra.repository.support.SimpleCassandraRepository;

import com.itau.welcometohellth.models.CarteirinhaVacina;
import com.itau.welcometohellth.models.Vacina;
import com.itau.welcometohellth.models.VacinaDose;

public class VacinaRepositoryImpl extends SimpleCassandraRepository<Vacina, UUID> implements VacinaRepository{
	private final CassandraOperations operations;
	
	@Autowired
	CarteirinhaVacinaRepository carteirinhaVacinaRepository;
	
	@Autowired
	VacinaDoseRepository vacinaDoseRepository;
	
	@Autowired
	DoseRepository doseRepository;
	
	public VacinaRepositoryImpl(CassandraEntityInformation<Vacina, UUID> metadata, CassandraOperations operations) {
		super(metadata, operations);
		this.operations = operations;
	}

	public <S extends Vacina> S deletar(String userEmail, UUID vacinaUuid) {
		
//		List<CarteirinhaVacina> carteirinhaVacinas = carteirinhaVacinaRepository.findAllByUserEmail(userEmail);
//		if (carteirinhaVacinas.isEmpty()) {
//			return null;
//		}
//		
//		CarteirinhaVacina carteirinhaVacina = new CarteirinhaVacina();
//		System.out.println(carteirinhaVacinas.size());
//		for (int i = 0; i < carteirinhaVacinas.size() ; i++) {
//			System.out.println("carteirinhaVacina.get (" + i + "): " + carteirinhaVacinas.get(i).getVacinaUuid());
//			System.out.println("vacinaUuid: " + vacinaUuid);
//			if (carteirinhaVacinas.get(i).getVacinaUuid().equals(vacinaUuid)) {
//				carteirinhaVacina = carteirinhaVacinas.get(i);
//				System.out.println("achou");
//			}
//		}
		
		Optional<Vacina> vacinaEncontrada = findById(vacinaUuid);
		if (!vacinaEncontrada.isPresent()) {
			return null;
		}
		
		Optional<CarteirinhaVacina> carteirinhaVacinaEncontrada = carteirinhaVacinaRepository.findByUserEmailAndVacinaUuid(userEmail, vacinaUuid);
		if (!carteirinhaVacinaEncontrada.isPresent()) {
			return null;
		}
		
		List<VacinaDose> vacinasDose = vacinaDoseRepository.findAllByVacinaUuid(vacinaUuid);
		
		for (VacinaDose dose : vacinasDose) {
			doseRepository.deletar(dose.getVacinaUuid(),dose.getDoseUuid());
		}
		
		CassandraBatchOperations ops = this.operations.batchOps();
		
		ops.delete(carteirinhaVacinaEncontrada.get());
		ops.delete(vacinaEncontrada.get());
		ops.execute();
		
		return (S)vacinaEncontrada.get();
	}
	
	public <S extends Vacina> S insert(String userEmail, S vacina) {
		CarteirinhaVacina carteirinhaVacina = new CarteirinhaVacina();
		carteirinhaVacina.setUserEmail(userEmail);
		carteirinhaVacina.setVacinaUuid(vacina.getVacinaUuid());
		carteirinhaVacina.setDataVacinacao(vacina.getDataVacinacao());
		carteirinhaVacina.setDescricao(vacina.getDescricao());
		carteirinhaVacina.setLaboratorio(vacina.getLaboratiorio());
		carteirinhaVacina.setLote(vacina.getLote());
		carteirinhaVacina.setNome(vacina.getNome());
		carteirinhaVacina.setNomeVacinador(vacina.getNomeVacinador());
		carteirinhaVacina.setPeriodoDias(vacina.getPeriodoDias());
		carteirinhaVacina.setQtdeDoses(vacina.getQtdeDoses());
		
		CassandraBatchOperations ops = this.operations.batchOps();
		
		ops.insert(vacina);
		ops.insert(carteirinhaVacina);
		ops.execute();
		
		return vacina;
	}
}