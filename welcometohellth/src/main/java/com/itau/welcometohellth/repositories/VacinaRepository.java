package com.itau.welcometohellth.repositories;

import java.util.UUID;

import org.springframework.data.cassandra.repository.CassandraRepository;
import org.springframework.data.repository.NoRepositoryBean;

import com.itau.welcometohellth.models.Vacina;

@NoRepositoryBean
public interface VacinaRepository extends CassandraRepository<Vacina, UUID>{

	public <S extends Vacina> S insert(String email, S entity);

	public <S extends Vacina> S deletar(String userEmail, UUID vacinaUuid);

}
