package com.itau.welcometohellth.repositories;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.welcometohellth.models.Carteirinha;
import com.itau.welcometohellth.models.CarteirinhaVacina;

public interface CarteirinhaRepository extends CrudRepository<Carteirinha, String> {

	public Optional<Carteirinha> findByEmailUsuario (String emailUsuario);
}
