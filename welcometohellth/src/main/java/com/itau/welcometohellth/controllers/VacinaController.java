package com.itau.welcometohellth.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.welcometohellth.models.Carteirinha;
import com.itau.welcometohellth.models.CarteirinhaVacina;
import com.itau.welcometohellth.models.Vacina;
import com.itau.welcometohellth.repositories.CarteirinhaRepository;
import com.itau.welcometohellth.repositories.CarteirinhaVacinaRepository;
import com.itau.welcometohellth.repositories.VacinaRepository;

@RestController
@CrossOrigin
public class VacinaController {

	@Autowired
	VacinaRepository vacinaRepository;

	@Autowired
	CarteirinhaRepository carteirinhaRepository;
	
	@Autowired
	CarteirinhaVacinaRepository carteirinhaVacinaRepository;
	
	@RequestMapping(method=RequestMethod.GET, path="/vacina/{uuid}")
	public ResponseEntity<Vacina> getVacina(@PathVariable UUID uuid) {
		Optional<Vacina> vacinaEncontrada = vacinaRepository.findById(uuid);	
		if (!vacinaEncontrada.isPresent()) {
			ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(vacinaEncontrada.get());
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/vacinas")
	public Iterable<Vacina> getVacinas() {
		return vacinaRepository.findAll();	
	}

	@RequestMapping(method=RequestMethod.GET, path="/vacinas/{userEmail}")
	public ResponseEntity<Iterable<CarteirinhaVacina>> getVacinasPorEmail(@PathVariable String userEmail) {
		Optional<Carteirinha> carteirinhaEncontrada = carteirinhaRepository.findByEmailUsuario(userEmail);
		if (!carteirinhaEncontrada.isPresent()) {
			ResponseEntity.notFound().build();
		}
		
		List<CarteirinhaVacina> vacinasEncontradas = carteirinhaVacinaRepository.findAllByUserEmail(userEmail);
		return ResponseEntity.ok(vacinasEncontradas);	
	}	
	
	@RequestMapping(method=RequestMethod.POST, path="/vacina")
	public ResponseEntity<Vacina> criar(@Valid @RequestBody Vacina vacina) {
	
		vacina.setVacinaUuid(UUID.randomUUID());
		Vacina vacinaInserida = vacinaRepository.insert(vacina.getUserEmail(), vacina);
		
		return ResponseEntity.ok(vacinaInserida);
	}	
	
	@RequestMapping(method=RequestMethod.PUT, path="/vacina/{uuid}")
	public ResponseEntity<Vacina> criar(@Valid @PathVariable UUID uuid, @RequestBody Vacina vacina) {
		
		Optional<Vacina> vacinaEncontrada = vacinaRepository.findById(uuid);			
		if (!vacinaEncontrada.isPresent()) {
    		return ResponseEntity.notFound().build();
		}
		
		Vacina vacinaAlterada = vacinaRepository.insert(vacinaEncontrada.get().getUserEmail(), vacinaEncontrada.get());
		
		return ResponseEntity.ok(vacinaAlterada);
	}	
	
	@RequestMapping(method=RequestMethod.DELETE, path="/vacina/{uuid}")
	public ResponseEntity<Vacina> deletar(@PathVariable UUID uuid) {
		Optional<Vacina> vacinaEncontrada = vacinaRepository.findById(uuid);
		if(!vacinaEncontrada.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		vacinaRepository.deletar(vacinaEncontrada.get().getUserEmail(),uuid);
		return ResponseEntity.ok(vacinaEncontrada.get());
	}
}

