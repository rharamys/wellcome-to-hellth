package com.itau.welcometohellth.controllers;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.welcometohellth.models.Carteirinha;
import com.itau.welcometohellth.models.Usuario;
import com.itau.welcometohellth.repositories.UsuarioRepository;
import com.itau.welcometohellth.services.JWTService;
import com.itau.welcometohellth.services.PasswordService;


@RestController
@CrossOrigin
public class UsuarioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	PasswordService passwordService;
	
	@Autowired
	JWTService jwtService;
	
	@RequestMapping(method=RequestMethod.POST, path="/usuario/add")
	public Usuario registrarUsuario(@Valid @RequestBody Usuario usuario) {

		String hash = passwordService.gerarHash(usuario.getSenha());
		usuario.setSenha(hash);
		
		usuarioRepository.save(usuario);
		usuario.setSenha(null);
		
		return usuario;
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/usuario/sessao")
	public ResponseEntity<?> getUsuarioByToken(@RequestHeader (value="Authorization") String authorization) {
	
		authorization = authorization.replace("Bearer ", "");
		String userEmailToken = jwtService.validarToken(authorization);
		if(userEmailToken == null) {
			return ResponseEntity.status(403).build();
		}
		
		Optional<Usuario> usuarioOptional = usuarioRepository.findByEmail(userEmailToken);
		if(!usuarioOptional.isPresent()) {
			return ResponseEntity.status(404).build();
		}
		
		return ResponseEntity.status(200).body(usuarioOptional.get());
		
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/usuario/login")
	public ResponseEntity<?> fazerLogin(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioBancoOptional = usuarioRepository.findByEmail(usuario.getEmail());
		if(!usuarioBancoOptional.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		
		Usuario usuarioBanco = usuarioBancoOptional.get(); 
		
		if(passwordService.verificarHash(usuario.getSenha(), usuarioBanco.getSenha())) {
			String token = jwtService.gerarToken(usuarioBanco.getEmail());
			
			HttpHeaders headers = new HttpHeaders();
			headers.add("Authorization", String.format("Bearer %s", token));
			headers.add("Access-Control-Expose-Headers", "Authorization");
			
			return new ResponseEntity<Usuario>(usuarioBanco, headers, HttpStatus.OK);
		}
		
		return ResponseEntity.badRequest().build();
	}

	@RequestMapping(method=RequestMethod.GET, path="/usuarios")
	public Iterable<?> buscarUsuarios() {
		
		return usuarioRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/usuario/{user}")
	public ResponseEntity<Usuario> buscarUsuario(@PathVariable String user) {
		Optional<Usuario> usuarioEncontrado = usuarioRepository.findByEmail(user);
		if (!usuarioEncontrado.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(usuarioEncontrado.get());
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/usuario")
	public ResponseEntity<Usuario> buscarUsuarioPeloToken(@PathVariable String user) {
		Optional<Usuario> usuarioEncontrado = usuarioRepository.findByEmail(user);
		if (!usuarioEncontrado.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(usuarioEncontrado.get());
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/usuario/{user}")
	public ResponseEntity<Usuario> deletarUsuario(@PathVariable String user) {
		Optional<Usuario> usuarioEncontrado = usuarioRepository.findByEmail(user);
		if (!usuarioEncontrado.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		usuarioRepository.deleteByEmail(user);
		return ResponseEntity.ok(usuarioEncontrado.get());
	}
//
//	public ResponseEntity<?> validarToken(@RequestHeader (value="Authorization") String authorization, @RequestBody String s) {
//		
//		authorization = authorization.replace("Bearer ", "");
//		String username = jwtService.validarToken(authorization);
//		if(username == null) {
//			return ResponseEntity.status(403).build();
//		}
//		
//		Optional<Usuario> usuarioOptional = usuarioRepository.findByEmail(username);
//		if(!usuarioOptional.isPresent()) {
//			return ResponseEntity.status(403).build();
//		}
//		
//		return ResponseEntity.status(HttpStatus.OK).body("");
//	}
}
