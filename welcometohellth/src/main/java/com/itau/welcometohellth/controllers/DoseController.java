package com.itau.welcometohellth.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.welcometohellth.models.VacinaDose;
import com.itau.welcometohellth.models.CarteirinhaVacina;
import com.itau.welcometohellth.models.Dose;
import com.itau.welcometohellth.models.Vacina;
import com.itau.welcometohellth.repositories.DoseRepository;
import com.itau.welcometohellth.repositories.VacinaDoseRepository;
import com.itau.welcometohellth.repositories.VacinaRepository;

@RestController
@CrossOrigin
public class DoseController {

	
		@Autowired
		DoseRepository doseRepository;
		
		@Autowired
		VacinaRepository vacinaRepository;
		
		@Autowired
		VacinaDoseRepository vacinaDoseRepository;
		
		@RequestMapping(method=RequestMethod.GET, path="/dose/{uuid}")
		public ResponseEntity<Dose> getDose(@PathVariable UUID uuid) {
			Optional<Dose> doseEncontrada = doseRepository.findById(uuid);	
			if (!doseEncontrada.isPresent()) {
				ResponseEntity.notFound().build();
			}
			
			return ResponseEntity.ok(doseEncontrada.get());
		}
		
		@RequestMapping(method=RequestMethod.GET, path="/doses")
		public Iterable<Dose> getDoses() {
			return doseRepository.findAll();	
		}

		@RequestMapping(method=RequestMethod.GET, path="/doses/{uuid}")
		public ResponseEntity<Iterable<VacinaDose>> getDosesPorVacina(@PathVariable UUID uuid) {
			Optional<Vacina> vacinaEncontrada = vacinaRepository.findById(uuid);
			if (!vacinaEncontrada.isPresent()) {
				ResponseEntity.notFound().build();
			}
			
			List<VacinaDose> dosesEncontradas = vacinaDoseRepository.findAllByVacinaUuid(uuid);
			return ResponseEntity.ok(dosesEncontradas);	
		}	
		
		@RequestMapping(method=RequestMethod.POST, path="/dose")
		public ResponseEntity<Dose> criar(@Valid @RequestBody Dose dose) {
			
			dose.setVacinaUuid(UUID.randomUUID());
			Dose doseInserida = doseRepository.insert(dose.getVacinaUuid(), dose);
			
			return ResponseEntity.ok(doseInserida);
		}	
		
		@RequestMapping(method=RequestMethod.PUT, path="/dose/{uuid}")
		public ResponseEntity<Dose> criar(@Valid @PathVariable UUID uuid, @RequestBody Dose dose) {
			
			//Date data = new Date(System.currentTimeMillis());
			
			Optional<Dose> doseEncontrada = doseRepository.findById(uuid);			
			if (!doseEncontrada.isPresent()) {
	    		return ResponseEntity.notFound().build();
			}
			
			dose.setDoseUuid(doseEncontrada.get().getDoseUuid());
			dose.setVacinaUuid(doseEncontrada.get().getVacinaUuid());
			Dose doseAlterada = doseRepository.insert(doseEncontrada.get().getVacinaUuid(), dose);
			
			return ResponseEntity.ok(doseAlterada);
		}	
		
		@RequestMapping(method=RequestMethod.DELETE, path="/dose/{uuid}")
		public ResponseEntity<Dose> deletar(@PathVariable UUID uuid) {
			Optional<Dose> doseEncontrada = doseRepository.findById(uuid);
			if(!doseEncontrada.isPresent()) {
				return ResponseEntity.notFound().build();
			}
			doseRepository.deletar(doseEncontrada.get().getVacinaUuid(),uuid);
			return ResponseEntity.ok(doseEncontrada.get());
		}
}
