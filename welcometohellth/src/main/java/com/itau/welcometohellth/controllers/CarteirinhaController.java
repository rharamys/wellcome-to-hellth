package com.itau.welcometohellth.controllers;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.welcometohellth.models.Carteirinha;
import com.itau.welcometohellth.models.CarteirinhaRetorno;
import com.itau.welcometohellth.models.Dose;
import com.itau.welcometohellth.models.Usuario;
import com.itau.welcometohellth.models.Vacina;
import com.itau.welcometohellth.repositories.CarteirinhaRepository;
import com.itau.welcometohellth.repositories.DoseRepository;
import com.itau.welcometohellth.repositories.UsuarioRepository;
import com.itau.welcometohellth.repositories.VacinaRepository;
import com.itau.welcometohellth.services.CarteirinhaRetornoBuilder;
import com.itau.welcometohellth.services.JWTService;

@RestController
@CrossOrigin
public class CarteirinhaController {

	@Autowired
	UsuarioRepository usuarioRepository;
	
	@Autowired
	CarteirinhaRepository carteirinhaRepository;
	
	@Autowired
	VacinaRepository vacinaRepository;
	
	@Autowired
	DoseRepository doseRepository;
	
	@Autowired
	JWTService jwtService;
	
	@Autowired
	CarteirinhaRetornoBuilder carteirinhaRetornoBuilder;

	@RequestMapping(method=RequestMethod.GET, path="/carteirinha/{userEmail}")
	public ResponseEntity<CarteirinhaRetorno> getCarteirinha(@PathVariable String userEmail) {
		Optional<Carteirinha> carteirinhaTalvez = carteirinhaRepository.findByEmailUsuario(userEmail);	
		if (!carteirinhaTalvez.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		Carteirinha carteirinhaEncontrada = carteirinhaTalvez.get();
		CarteirinhaRetorno retorno = carteirinhaRetornoBuilder.build(userEmail, carteirinhaEncontrada.getTipoCarteirinha());
		
		return ResponseEntity.ok(retorno);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/carteirinhas")
	public Iterable<Carteirinha> getCarteirinhas() {
		return carteirinhaRepository.findAll();	
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/carteirinha")
	public ResponseEntity<Carteirinha> gerar(@RequestHeader (value="Authorization") String authorization, 
			@Valid @RequestBody Carteirinha carteirinha) {
		
		authorization = authorization.replace("Bearer ", "");
		String userEmailToken = jwtService.validarToken(authorization);
		if(userEmailToken == null) {
			return ResponseEntity.status(403).build();
		}
		
		Optional<Usuario> usuarioOptional = usuarioRepository.findByEmail(userEmailToken);
		if(!usuarioOptional.isPresent()) {
			return ResponseEntity.status(403).build();
		}
		
		carteirinha.setEmailUsuario(usuarioOptional.get().getEmail());
		Carteirinha carteirinhaInserida = carteirinhaRepository.save(carteirinha);
		
		Vacina vacina1 = new Vacina();
		vacina1.setVacinaUuid(UUID.randomUUID());
		vacina1.setUserEmail(userEmailToken);
		vacina1.setDataVacinacao("");
		vacina1.setDescricao("Vacina de Rubeola");
		vacina1.setLaboratiorio("");
		vacina1.setLote(1);
		vacina1.setNome("Rubeola");
		vacina1.setNomeVacinador("");
		vacina1.setPeriodoDias(256);
		vacina1.setQtdeDoses(2);
		vacinaRepository.insert(userEmailToken, vacina1);
		
		Dose dose1Rubeola = new Dose();
		dose1Rubeola.setDoseUuid(UUID.randomUUID());
		dose1Rubeola.setVacinaUuid(vacina1.getVacinaUuid());
		dose1Rubeola.setDataDose("");
		dose1Rubeola.setDataInjecao("");
		dose1Rubeola.setTomada(false);
		doseRepository.insert(vacina1.getVacinaUuid(), dose1Rubeola);
		
		Dose dose2Rubeola = new Dose();
		dose2Rubeola.setDoseUuid(UUID.randomUUID());
		dose2Rubeola.setVacinaUuid(vacina1.getVacinaUuid());
		dose2Rubeola.setDataDose("");
		dose2Rubeola.setDataInjecao("");
		dose2Rubeola.setTomada(false);
		doseRepository.insert(vacina1.getVacinaUuid(), dose2Rubeola);
		
		Vacina vacina2 = new Vacina();
		vacina2.setVacinaUuid(UUID.randomUUID());
		vacina2.setUserEmail(userEmailToken);
		vacina2.setDataVacinacao("");
		vacina2.setDescricao("Vacina de Febre Amarela");
		vacina2.setLaboratiorio("");
		vacina2.setLote(1);
		vacina2.setNome("Febre Amarela");
		vacina2.setNomeVacinador("");
		vacina2.setPeriodoDias(0);
		vacina2.setQtdeDoses(1);
		vacinaRepository.insert(userEmailToken, vacina2);
		
		Dose dose1Febre = new Dose();
		dose1Febre.setDoseUuid(UUID.randomUUID());
		dose1Febre.setVacinaUuid(vacina2.getVacinaUuid());
		dose1Febre.setDataDose("");
		dose1Febre.setDataInjecao("");
		dose1Febre.setTomada(false);
		doseRepository.insert(vacina2.getVacinaUuid(), dose1Febre);
		
		Vacina vacina3 = new Vacina();
		vacina3.setVacinaUuid(UUID.randomUUID());
		vacina3.setUserEmail(userEmailToken);
		vacina3.setDataVacinacao("");
		vacina3.setDescricao("Vacina de Varíola");
		vacina3.setLaboratiorio("");
		vacina3.setLote(1);
		vacina3.setNome("Varíola");
		vacina3.setNomeVacinador("");
		vacina3.setPeriodoDias(90);
		vacina3.setQtdeDoses(3);
		vacinaRepository.insert(userEmailToken, vacina3);
		
		Dose dose1Variola = new Dose();
		dose1Variola.setDoseUuid(UUID.randomUUID());
		dose1Variola.setVacinaUuid(vacina3.getVacinaUuid());
		dose1Variola.setDataDose("");
		dose1Variola.setDataInjecao("");
		dose1Variola.setTomada(false);
		doseRepository.insert(vacina3.getVacinaUuid(), dose1Variola);
		
		Dose dose2Variola = new Dose();
		dose2Variola.setDoseUuid(UUID.randomUUID());
		dose2Variola.setVacinaUuid(vacina3.getVacinaUuid());
		dose2Variola.setDataDose("");
		dose2Variola.setDataInjecao("");
		dose2Variola.setTomada(false);
		doseRepository.insert(vacina3.getVacinaUuid(), dose2Variola);
		
		Dose dose3Variola = new Dose();
		dose3Variola.setDoseUuid(UUID.randomUUID());
		dose3Variola.setVacinaUuid(vacina3.getVacinaUuid());
		dose3Variola.setDataDose("");
		dose3Variola.setDataInjecao("");
		dose3Variola.setTomada(false);
		doseRepository.insert(vacina3.getVacinaUuid(), dose3Variola);
		
		Vacina vacina4 = new Vacina();
		vacina4.setVacinaUuid(UUID.randomUUID());
		vacina4.setUserEmail(userEmailToken);
		vacina4.setDataVacinacao("");
		vacina4.setDescricao("Dose única - ao nascer");
		vacina4.setLaboratiorio("");
		vacina4.setLote(1);
		vacina4.setNome("BCG");
		vacina4.setNomeVacinador("");
		vacina4.setPeriodoDias(0);
		vacina4.setQtdeDoses(1);
		vacinaRepository.insert(userEmailToken, vacina4);
		
		Dose dose1BCG = new Dose();
		dose1BCG.setDoseUuid(UUID.randomUUID());
		dose1BCG.setVacinaUuid(vacina4.getVacinaUuid());
		dose1BCG.setDataDose("");
		dose1BCG.setDataInjecao("");
		dose1BCG.setTomada(false);
		doseRepository.insert(vacina4.getVacinaUuid(), dose1BCG);
				
		Vacina vacina5 = new Vacina();
		vacina5.setVacinaUuid(UUID.randomUUID());
		vacina5.setUserEmail(userEmailToken);
		vacina5.setDataVacinacao("");
		vacina5.setDescricao("Dose única - ao nascer");
		vacina5.setLaboratiorio("");
		vacina5.setLote(1);
		vacina5.setNome("Hepatite B");
		vacina5.setNomeVacinador("");
		vacina5.setPeriodoDias(0);
		vacina5.setQtdeDoses(1);
		vacinaRepository.insert(userEmailToken, vacina5);
		
		Dose dose1HepatiteB = new Dose();
		dose1HepatiteB.setDoseUuid(UUID.randomUUID());
		dose1HepatiteB.setVacinaUuid(vacina5.getVacinaUuid());
		dose1HepatiteB.setDataDose("");
		dose1HepatiteB.setDataInjecao("");
		dose1HepatiteB.setTomada(false);
		doseRepository.insert(vacina5.getVacinaUuid(), dose1HepatiteB);
		
		Vacina vacina6 = new Vacina();
		vacina6.setVacinaUuid(UUID.randomUUID());
		vacina6.setUserEmail(userEmailToken);
		vacina6.setDataVacinacao("");
		vacina6.setDescricao("Pentavalente (DTPa-VIP/Hib)");
		vacina6.setLaboratiorio("");
		vacina6.setLote(1);
		vacina6.setNome("Penta/DTP");
		vacina6.setNomeVacinador("");
		vacina6.setPeriodoDias(60);
		vacina6.setQtdeDoses(3);
		vacinaRepository.insert(userEmailToken, vacina6);
		
		Dose dose1PentaDTP = new Dose();
		dose1PentaDTP.setDoseUuid(UUID.randomUUID());
		dose1PentaDTP.setVacinaUuid(vacina6.getVacinaUuid());
		dose1PentaDTP.setDataDose("");
		dose1PentaDTP.setDataInjecao("");
		dose1PentaDTP.setTomada(false);
		doseRepository.insert(vacina6.getVacinaUuid(), dose1PentaDTP);
		
		Dose dose2PentaDTP = new Dose();
		dose2PentaDTP.setDoseUuid(UUID.randomUUID());
		dose2PentaDTP.setVacinaUuid(vacina6.getVacinaUuid());
		dose2PentaDTP.setDataDose("");
		dose2PentaDTP.setDataInjecao("");
		dose2PentaDTP.setTomada(false);
		doseRepository.insert(vacina6.getVacinaUuid(), dose2PentaDTP);
		
		Dose dose3PentaDTP = new Dose();
		dose3PentaDTP.setDoseUuid(UUID.randomUUID());
		dose3PentaDTP.setVacinaUuid(vacina6.getVacinaUuid());
		dose3PentaDTP.setDataDose("");
		dose3PentaDTP.setDataInjecao("");
		dose3PentaDTP.setTomada(false);
		doseRepository.insert(vacina6.getVacinaUuid(), dose3PentaDTP);
		
		
		Vacina vacina7 = new Vacina();
		vacina7.setVacinaUuid(UUID.randomUUID());
		vacina7.setUserEmail(userEmailToken);
		vacina7.setDataVacinacao("");
		vacina7.setDescricao("Vacina Inativada Poliomielite / Vacina Oral Poliomielite");
		vacina7.setLaboratiorio("");
		vacina7.setLote(1);
		vacina7.setNome("VIP/VOP");
		vacina7.setNomeVacinador("");
		vacina7.setPeriodoDias(60);
		vacina7.setQtdeDoses(3);
		vacinaRepository.insert(userEmailToken, vacina7);
		
		Dose dose1VIPVOP = new Dose();
		dose1VIPVOP.setDoseUuid(UUID.randomUUID());
		dose1VIPVOP.setVacinaUuid(vacina7.getVacinaUuid());
		dose1VIPVOP.setDataDose("");
		dose1VIPVOP.setDataInjecao("");
		dose1VIPVOP.setTomada(false);
		doseRepository.insert(vacina7.getVacinaUuid(), dose1VIPVOP);
		
		Dose dose2VIPVOP = new Dose();
		dose2VIPVOP.setDoseUuid(UUID.randomUUID());
		dose2VIPVOP.setVacinaUuid(vacina7.getVacinaUuid());
		dose2VIPVOP.setDataDose("");
		dose2VIPVOP.setDataInjecao("");
		dose2VIPVOP.setTomada(false);
		doseRepository.insert(vacina7.getVacinaUuid(), dose2VIPVOP);
		
		Dose dose3VIPVOP = new Dose();
		dose3VIPVOP.setDoseUuid(UUID.randomUUID());
		dose3VIPVOP.setVacinaUuid(vacina7.getVacinaUuid());
		dose3VIPVOP.setDataDose("");
		dose3VIPVOP.setDataInjecao("");
		dose3VIPVOP.setTomada(false);
		doseRepository.insert(vacina7.getVacinaUuid(), dose3VIPVOP);
		
		
		Dose dose4VIPVOP = new Dose();
		dose4VIPVOP.setDoseUuid(UUID.randomUUID());
		dose4VIPVOP.setVacinaUuid(vacina7.getVacinaUuid());
		dose4VIPVOP.setDataDose("");
		dose4VIPVOP.setDataInjecao("");
		dose4VIPVOP.setTomada(false);
		doseRepository.insert(vacina7.getVacinaUuid(), dose4VIPVOP);
		
		Dose dose5VIPVOP = new Dose();
		dose5VIPVOP.setDoseUuid(UUID.randomUUID());
		dose5VIPVOP.setVacinaUuid(vacina7.getVacinaUuid());
		dose5VIPVOP.setDataDose("");
		dose5VIPVOP.setDataInjecao("");
		dose5VIPVOP.setTomada(false);
		doseRepository.insert(vacina7.getVacinaUuid(), dose5VIPVOP);
		
		Vacina vacina8 = new Vacina();
		vacina8.setVacinaUuid(UUID.randomUUID());
		vacina8.setUserEmail(userEmailToken);
		vacina8.setDataVacinacao("");
		vacina8.setDescricao("Pneumocócica conjugada 10-valente (VPC10)");
		vacina8.setLaboratiorio("");
		vacina8.setLote(1);
		vacina8.setNome("VCP10");
		vacina8.setNomeVacinador("");
		vacina8.setPeriodoDias(60);
		vacina8.setQtdeDoses(2);
		vacinaRepository.insert(userEmailToken, vacina8);
		
		Dose dose1VCP10 = new Dose();
		dose1VCP10.setDoseUuid(UUID.randomUUID());
		dose1VCP10.setVacinaUuid(vacina8.getVacinaUuid());
		dose1VCP10.setDataDose("");
		dose1VCP10.setDataInjecao("");
		dose1VCP10.setTomada(false);
		doseRepository.insert(vacina8.getVacinaUuid(), dose1VCP10);
		
		Dose dose2VCP10 = new Dose();
		dose2VCP10.setDoseUuid(UUID.randomUUID());
		dose2VCP10.setVacinaUuid(vacina8.getVacinaUuid());
		dose2VCP10.setDataDose("");
		dose2VCP10.setDataInjecao("");
		dose2VCP10.setTomada(false);
		doseRepository.insert(vacina8.getVacinaUuid(), dose2VCP10);
		
		Vacina vacina9 = new Vacina();
		vacina9.setVacinaUuid(UUID.randomUUID());
		vacina9.setUserEmail(userEmailToken);
		vacina9.setDataVacinacao("");
		vacina9.setDescricao("Protege da bactéria meningocóco C");
		vacina9.setLaboratiorio("");
		vacina9.setLote(1);
		vacina9.setNome("Meningocócica C");
		vacina9.setNomeVacinador("");
		vacina9.setPeriodoDias(60);
		vacina9.setQtdeDoses(2);
		vacinaRepository.insert(userEmailToken, vacina9);
		
		Dose dose1MC = new Dose();
		dose1MC.setDoseUuid(UUID.randomUUID());
		dose1MC.setVacinaUuid(vacina9.getVacinaUuid());
		dose1MC.setDataDose("");
		dose1MC.setDataInjecao("");
		dose1MC.setTomada(false);
		doseRepository.insert(vacina9.getVacinaUuid(), dose1MC);
		
		Dose dose2MC = new Dose();
		dose2MC.setDoseUuid(UUID.randomUUID());
		dose2MC.setVacinaUuid(vacina9.getVacinaUuid());
		dose2MC.setDataDose("");
		dose2MC.setDataInjecao("");
		dose2MC.setTomada(false);
		doseRepository.insert(vacina9.getVacinaUuid(), dose2MC);
		
		
		Vacina vacina10 = new Vacina();
		vacina10.setVacinaUuid(UUID.randomUUID());
		vacina10.setUserEmail(userEmailToken);
		vacina10.setDataVacinacao("");
		vacina10.setDescricao("Protege contra a Hepatite A 3");
		vacina10.setLaboratiorio("");
		vacina10.setLote(1);
		vacina10.setNome("Hepatite A");
		vacina10.setNomeVacinador("");
		vacina10.setPeriodoDias(0);
		vacina10.setQtdeDoses(1);
		vacinaRepository.insert(userEmailToken, vacina10);
		
		Dose dose1HPA = new Dose();
		dose1HPA.setDoseUuid(UUID.randomUUID());
		dose1HPA.setVacinaUuid(vacina10.getVacinaUuid());
		dose1HPA.setDataDose("");
		dose1HPA.setDataInjecao("");
		dose1HPA.setTomada(false);
		doseRepository.insert(vacina10.getVacinaUuid(), dose1HPA);
		
		
		Vacina vacina11 = new Vacina();
		vacina11.setVacinaUuid(UUID.randomUUID());
		vacina11.setUserEmail(userEmailToken);
		vacina11.setDataVacinacao("");
		vacina11.setDescricao("Protege contra Sarampo, Caxumba e Rubéola");
		vacina11.setLaboratiorio("");
		vacina11.setLote(1);
		vacina11.setNome("Tríplice Viral");
		vacina11.setNomeVacinador("");	
		vacina11.setPeriodoDias(0);
		vacina11.setQtdeDoses(1);
		vacinaRepository.insert(userEmailToken, vacina11);
		
		Dose dose1TPV = new Dose();
		dose1TPV.setDoseUuid(UUID.randomUUID());
		dose1TPV.setVacinaUuid(vacina11.getVacinaUuid());
		dose1TPV.setDataDose("");
		dose1TPV.setDataInjecao("");
		dose1TPV.setTomada(false);
		doseRepository.insert(vacina11.getVacinaUuid(), dose1TPV);
		
		Vacina vacina12 = new Vacina();
		vacina12.setVacinaUuid(UUID.randomUUID());
		vacina12.setUserEmail(userEmailToken);
		vacina12.setDataVacinacao("");
		vacina12.setDescricao("Papiloma vírus humano");
		vacina12.setLaboratiorio("");
		vacina12.setLote(1);
		vacina12.setNome("HPV");
		vacina12.setNomeVacinador("");	
		vacina12.setPeriodoDias(0);
		vacina12.setQtdeDoses(1);
		vacinaRepository.insert(userEmailToken, vacina12);
		
		Dose dose1HPV = new Dose();
		dose1HPV.setDoseUuid(UUID.randomUUID());
		dose1HPV.setVacinaUuid(vacina12.getVacinaUuid());
		dose1HPV.setDataDose("");
		dose1HPV.setDataInjecao("");
		dose1HPV.setTomada(false);
		doseRepository.insert(vacina12.getVacinaUuid(), dose1HPV);
		
		
		return ResponseEntity.ok(carteirinhaInserida);
	}	
	
	@RequestMapping(method=RequestMethod.PUT, path="/carteirinha/{userEmail}")
	public ResponseEntity<Carteirinha> atualizar(@RequestHeader (value="Authorization") String authorization,
			@Valid @PathVariable String userEmail, @RequestBody Carteirinha carteirinha) {
		authorization = authorization.replace("Bearer ", "");
		String userEmailToken = jwtService.validarToken(authorization);
		if(userEmailToken == null) {
			return ResponseEntity.status(403).build();
		}
		Optional<Usuario> usuarioOptional = usuarioRepository.findByEmail(userEmailToken);
		if(!usuarioOptional.isPresent()) {
			return ResponseEntity.status(403).build();
		}
		Optional<Carteirinha> carteirinhaEncontrada = carteirinhaRepository.findByEmailUsuario(userEmail);			
		if (!carteirinhaEncontrada.isPresent()) {
    		return ResponseEntity.notFound().build();
		}
		
		Carteirinha carteirinhaAlterada = carteirinhaRepository.save(carteirinha);
		
		return ResponseEntity.ok(carteirinhaAlterada);
	}	
	
	@RequestMapping(method=RequestMethod.DELETE, path="/carteirinha/{userEmail}")
	public ResponseEntity<Carteirinha> deletar(@RequestHeader (value="Authorization") String authorization,
			@PathVariable String userEmail) {
		authorization = authorization.replace("Bearer ", "");
		String userEmailToken = jwtService.validarToken(authorization);
		if(userEmailToken == null) {
			return ResponseEntity.status(403).build();
		}
		Optional<Usuario> usuarioOptional = usuarioRepository.findByEmail(userEmailToken);
		if(!usuarioOptional.isPresent()) {
			return ResponseEntity.status(403).build();
		}
		Optional<Carteirinha> carteirinhaEncontrada = carteirinhaRepository.findByEmailUsuario(userEmail);
		if(!carteirinhaEncontrada.isPresent()) {
			return ResponseEntity.notFound().build();
		}
		carteirinhaRepository.delete(carteirinhaEncontrada.get());
		return ResponseEntity.ok(carteirinhaEncontrada.get());
	}
}

