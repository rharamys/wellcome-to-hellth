package com.itau.welcometohellth.models;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class CarteirinhaVacina {

	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private String userEmail;
	
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private UUID vacinaUuid;
	
	@NotNull
	private String nome;
	
	@NotNull
	private int periodoDias;
	
	@NotNull
	private int qtdeDoses;
	
	@NotNull
	private String descricao;
	
	private String dataVacinacao;
	private int lote;
	private String laboratorio;
	private String nomeVacinador;
	
	
	public UUID getVacinaUuid() {
		return vacinaUuid;
	}
	public void setVacinaUuid(UUID vacinaUuid) {
		this.vacinaUuid = vacinaUuid;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getPeriodoDias() {
		return periodoDias;
	}
	public void setPeriodoDias(int periodoDias) {
		this.periodoDias = periodoDias;
	}
	public int getQtdeDoses() {
		return qtdeDoses;
	}
	public void setQtdeDoses(int qtdeDoses) {
		this.qtdeDoses = qtdeDoses;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDataVacinacao() {
		return dataVacinacao;
	}
	public void setDataVacinacao(String dataVacinacao) {
		this.dataVacinacao = dataVacinacao;
	}
	public int getLote() {
		return lote;
	}
	public void setLote(int lote) {
		this.lote = lote;
	}
	public String getLaboratorio() {
		return laboratorio;
	}
	public void setLaboratorio(String laboratorio) {
		this.laboratorio = laboratorio;
	}
	public String getNomeVacinador() {
		return nomeVacinador;
	}
	public void setNomeVacinador(String nomeVacinador) {
		this.nomeVacinador = nomeVacinador;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	
}
