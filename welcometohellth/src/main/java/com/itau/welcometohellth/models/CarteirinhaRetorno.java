package com.itau.welcometohellth.models;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.itau.welcometohellth.repositories.CarteirinhaVacinaRepository;

public class CarteirinhaRetorno {
	
	private String emailUsuario;
	private String tipoCarteirinha;
	private List<VacinaRetorno> vacinaRetornos = new ArrayList<>();

	public String getEmailUsuario() {
		return this.emailUsuario;
	}

	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}

	public String getTipoCarteirinha() {
		return tipoCarteirinha;
	}

	public void setTipoCarteirinha(String tipoCarteirinha) {
		this.tipoCarteirinha = tipoCarteirinha;
	}

	public void setVacinaRetornos(List<VacinaRetorno> vacinas) {
		this.vacinaRetornos = vacinas;
	}
	
	public List<VacinaRetorno> getVacinaRetornos() {
		return this.vacinaRetornos;
	}

}
