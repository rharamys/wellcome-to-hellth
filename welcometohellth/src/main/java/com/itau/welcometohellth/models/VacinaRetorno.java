package com.itau.welcometohellth.models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Autowired;

import com.itau.welcometohellth.repositories.VacinaDoseRepository;

public class VacinaRetorno {
	
	private UUID vacinaUuid;
	private String userEmail;
	private String nome;
	private int periodoDias;
	private int qtdeDoses;
	private String descricao;
	private String dataVacinacao;
	private int lote;
	private String laboratiorio;
	private String nomeVacinador;
	private List doses = new ArrayList();
	
	public List getDoses() {
		return this.doses;
	}

	public UUID getVacinaUuid() {
		return vacinaUuid;
	}

	public void setVacinaUuid(UUID vacinaUuid) {
		this.vacinaUuid = vacinaUuid;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getPeriodoDias() {
		return periodoDias;
	}

	public void setPeriodoDias(int periodoDias) {
		this.periodoDias = periodoDias;
	}

	public int getQtdeDoses() {
		return qtdeDoses;
	}

	public void setQtdeDoses(int qtdeDoses) {
		this.qtdeDoses = qtdeDoses;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDataVacinacao() {
		return dataVacinacao;
	}

	public void setDataVacinacao(String dataVacinacao) {
		this.dataVacinacao = dataVacinacao;
	}

	public int getLote() {
		return lote;
	}

	public void setLote(int lote) {
		this.lote = lote;
	}

	public String getLaboratiorio() {
		return laboratiorio;
	}

	public void setLaboratiorio(String laboratiorio) {
		this.laboratiorio = laboratiorio;
	}

	public String getNomeVacinador() {
		return nomeVacinador;
	}

	public void setNomeVacinador(String nomeVacinador) {
		this.nomeVacinador = nomeVacinador;
	}

	public void setDoses(List doses) {
		this.doses = doses;
	}

}
