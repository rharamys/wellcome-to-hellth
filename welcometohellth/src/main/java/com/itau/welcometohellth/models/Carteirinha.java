package com.itau.welcometohellth.models;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Carteirinha {
	
	@PrimaryKey
	private String emailUsuario;
	
	private String tipoCarteirinha;

	public String getEmailUsuario() {
		return emailUsuario;
	}

	public void setEmailUsuario(String emailUsuario) {
		this.emailUsuario = emailUsuario;
	}

	public String getTipoCarteirinha() {
		return tipoCarteirinha;
	}

	public void setTipoCarteirinha(String tipoCarteirinha) {
		this.tipoCarteirinha = tipoCarteirinha;
	}
	
	
}
