package com.itau.welcometohellth.models;

import java.util.UUID;

import org.springframework.data.annotation.Id;
import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.Indexed;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class VacinaDose {

	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private UUID vacinaUuid;
	
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private UUID doseUuid;
	
	private boolean tomada;
	private String dataInjecao;
	private String dataDose;
	
	private int lote;
	private String laboratorio;
	private String nomeVacinador;
	
	
	public int getLote() {
		return lote;
	}
	public void setLote(int lote) {
		this.lote = lote;
	}
	public String getLaboratorio() {
		return laboratorio;
	}
	public void setLaboratorio(String laboratorio) {
		this.laboratorio = laboratorio;
	}
	public String getNomeVacinador() {
		return nomeVacinador;
	}
	public void setNomeVacinador(String nomeVacinador) {
		this.nomeVacinador = nomeVacinador;
	}
	public UUID getDoseUuid() {
		return doseUuid;
	}
	public void setDoseUuid(UUID doseUuid) {
		this.doseUuid = doseUuid;
	}
	public String getDataDose() {
		return dataDose;
	}
	public void setDataDose(String dataDose) {
		this.dataDose = dataDose;
	}
	public boolean isTomada() {
		return tomada;
	}
	public void setTomada(boolean tomada) {
		this.tomada = tomada;
	}
	public String getDataInjecao() {
		return dataInjecao;
	}
	public void setDataInjecao(String dataInjecao) {
		this.dataInjecao = dataInjecao;
	}
	public UUID getVacinaUuid() {
		return vacinaUuid;
	}
	public void setVacinaUuid(UUID vacinaUuid) {
		this.vacinaUuid = vacinaUuid;
	}
	
}
