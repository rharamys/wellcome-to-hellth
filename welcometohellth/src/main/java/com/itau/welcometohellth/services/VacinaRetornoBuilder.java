package com.itau.welcometohellth.services;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.welcometohellth.models.VacinaRetorno;
import com.itau.welcometohellth.repositories.VacinaDoseRepository;

@Service
public class VacinaRetornoBuilder {

	@Autowired
	VacinaDoseRepository vacinaDoseRepository;
	
	public VacinaRetorno build(UUID vacinaUuid, 
			String userEmail, 
			String nome , 
			String descricao, 
			String dataVacinacao, 
			String laboratorio, 
			String nomeVacinador, 
			int periodoDias, 
			int qtdeDoses, 
			int lote) 
	{
		
		VacinaRetorno vacinaRetorno = new VacinaRetorno();
		
		vacinaRetorno.setDataVacinacao(dataVacinacao);
		vacinaRetorno.setDescricao(descricao);
		vacinaRetorno.setLaboratiorio(laboratorio);
		vacinaRetorno.setLote(lote);
		vacinaRetorno.setNome(nome);
		vacinaRetorno.setNomeVacinador(nomeVacinador);
		vacinaRetorno.setPeriodoDias(periodoDias);
		vacinaRetorno.setQtdeDoses(qtdeDoses);
		vacinaRetorno.setUserEmail(userEmail);
		vacinaRetorno.setVacinaUuid(vacinaUuid);
		
		List doses = vacinaDoseRepository.findAllByVacinaUuid(vacinaUuid);
//		for (int i = 0; i< doses.size() ; i++) {
//			doses.add(doses.get(i));		
//		}
		vacinaRetorno.setDoses(doses);
		
		return vacinaRetorno;
	}
}
