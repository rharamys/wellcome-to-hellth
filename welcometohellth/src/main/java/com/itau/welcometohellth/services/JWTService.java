package com.itau.welcometohellth.services;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;

@Service
public class JWTService {
	
	String key = "lojinha";
	Algorithm algorithm = Algorithm.HMAC256(key);
	JWTVerifier verifier = JWT.require(algorithm).build();
	
	public String gerarToken(String username) {
		return JWT.create().withClaim("username", username).sign(algorithm);
	}
	
	public String validarToken(String token) {
		try {
			return verifier.verify(token).getClaim("username").asString();
		}catch (Exception e) {
			return null;
		}
	}
}