package com.itau.welcometohellth.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itau.welcometohellth.models.CarteirinhaRetorno;
import com.itau.welcometohellth.models.CarteirinhaVacina;
import com.itau.welcometohellth.models.VacinaRetorno;
import com.itau.welcometohellth.repositories.CarteirinhaVacinaRepository;

@Service
public class CarteirinhaRetornoBuilder {
	@Autowired
	CarteirinhaVacinaRepository carteirinhaVacinaRepository;
	
	@Autowired
	VacinaRetornoBuilder vacinaRetornoBuilder;

	public CarteirinhaRetorno build(String emailUsuario, String tipo) {
		
		CarteirinhaRetorno retorno = new CarteirinhaRetorno();
		
		List<CarteirinhaVacina> vacinas = carteirinhaVacinaRepository.findAllByUserEmail(emailUsuario);
		
		List<VacinaRetorno> vacinaRetornos = new ArrayList<>();
		for (int i=0 ; i < vacinas.size(); i++) {
			
			VacinaRetorno vacinaRetorno = vacinaRetornoBuilder.build(
					vacinas.get(i).getVacinaUuid(),
					vacinas.get(i).getUserEmail(),
					vacinas.get(i).getNome(), 
					vacinas.get(i).getDescricao(), 
					vacinas.get(i).getDataVacinacao(), 
					vacinas.get(i).getLaboratorio(), 
					vacinas.get(i).getNomeVacinador(), 
					vacinas.get(i).getPeriodoDias(), 
					vacinas.get(i).getQtdeDoses(), 
					vacinas.get(i).getLote());
			
			vacinaRetornos.add(vacinaRetorno);
			
		}
		
		retorno.setEmailUsuario(emailUsuario);
		retorno.setTipoCarteirinha(tipo);
		retorno.setVacinaRetornos(vacinaRetornos);
		
		return retorno;
	}
}
