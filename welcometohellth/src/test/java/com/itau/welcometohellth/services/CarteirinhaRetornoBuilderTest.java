package com.itau.welcometohellth.services;

import static org.junit.Assert.assertEquals;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;


import com.itau.welcometohellth.models.CarteirinhaVacina;
import com.itau.welcometohellth.models.VacinaDose;
import com.itau.welcometohellth.models.VacinaRetorno;
import com.itau.welcometohellth.models.CarteirinhaRetorno;
import com.itau.welcometohellth.repositories.CarteirinhaVacinaRepository;
import com.itau.welcometohellth.repositories.VacinaDoseRepository;


@RunWith(SpringRunner.class)
@WebMvcTest(CarteirinhaRetornoBuilder.class)
public class CarteirinhaRetornoBuilderTest {
	
	@Autowired
	CarteirinhaRetornoBuilder carteirinhaRetornoBuilder;
	
	@MockBean
	VacinaRetornoBuilder vacinaRetornoBuilder;
	
	@MockBean
	CarteirinhaVacinaRepository carteirinhaVacinaRepository;
	
	@MockBean
	VacinaDoseRepository vacinaDoseRepository;
	
	ObjectMapper mapper = new ObjectMapper();
	
	@Test
	public void testarCarteirinhaBuilder() throws Exception {
		
		when(carteirinhaVacinaRepository.findAllByUserEmail(any(String.class))).thenReturn(new ArrayList<CarteirinhaVacina>());
		when(vacinaDoseRepository.findAllByVacinaUuid(any(UUID.class))).thenReturn(new ArrayList<VacinaDose>());
		when(vacinaRetornoBuilder.build(
				any(UUID.class),
				any(String.class),
				any(String.class), 
				any(String.class), 
				any(String.class), 
				any(String.class), 
				any(String.class), 
				any(int.class),  
				any(int.class), 
				any(int.class)))
		.thenReturn(new VacinaRetorno());
		
		
		CarteirinhaRetorno retorno = carteirinhaRetornoBuilder.build("douglera@teste.com", "carteirinhaNormal");

		assertEquals(retorno.getEmailUsuario(),"douglera@teste.com");
		assertEquals(retorno.getTipoCarteirinha(), "carteirinhaNormal");
		assertEquals(retorno.getVacinaRetornos().size(), 0);
	}
}
